import { createApp } from "vue";
import App from "./App.vue";

createApp(App).mount("#app");

export type PicsumAPIResponse = {
  id: string;
  author: string;
  width: number;
  height: number;
  url: string;
  download_url: string;
};
